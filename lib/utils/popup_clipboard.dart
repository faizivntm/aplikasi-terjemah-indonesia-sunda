import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:flutter/material.dart';

class PopupClipboard {
  static void showCopySuccessDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: primaryColor,
          content: Text(
            "Teks berhasil disalin",
            textAlign: TextAlign.center,
            style: primaryTextStyle.copyWith(
              fontSize: 15,
              fontWeight: semiBold,
              color: white,
            ),
          ),
        );
      },
    );
  }
}
