import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//Ganti warna-warna sesuai denga desain yang disediakan di figma
const Color primaryColor = Color(0xff27AE60);
const Color primary700 = Color(0xff17683A);
const Color primary950 = Color(0xff04110A);
const Color white = Color(0xffFEFFFE);
const Color lightGrey = Color(0xffD8D8D8);

//ganti gaya font sesuai dengan desain yang ada di figma
TextStyle primaryTextStyle = GoogleFonts.roboto();

const FontWeight light = FontWeight.w300;
const FontWeight reguler = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semiBold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
