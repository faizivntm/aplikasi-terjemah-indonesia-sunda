import 'package:flutter/foundation.dart';

class TranslationProvider with ChangeNotifier {
  String _inputText = '';

  String get inputText => _inputText;

  void setInputText(String text) {
    _inputText = text;
    notifyListeners();
  }
}
