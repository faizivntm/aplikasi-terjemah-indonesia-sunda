import 'package:applikasi_translator_indonesia_sunda/utils/popup_clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ClipboardProvider with ChangeNotifier {
  void copyToClipboard(String text, BuildContext context) async {
    await Clipboard.setData(ClipboardData(text: text));
    // ignore: use_build_context_synchronously
    showCopySuccessDialog(context);
  }

  void showCopySuccessDialog(BuildContext context) {
    PopupClipboard.showCopySuccessDialog(context);
  }
}
