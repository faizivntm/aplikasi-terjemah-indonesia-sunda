import 'package:applikasi_translator_indonesia_sunda/about_screen.dart';
import 'package:applikasi_translator_indonesia_sunda/home_screen.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyBottomNavigationBar extends StatefulWidget {
  const MyBottomNavigationBar({super.key});

  @override
  State<MyBottomNavigationBar> createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int selected = 0;
  List<Widget> item = [];

  void onTappedBar(int index) {
    setState(() {
      selected = index;
    });
  }

  @override
  void initState() {
    item = [const HomeScreen(), AboutScreen()];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: item[selected],
      bottomNavigationBar: Stack(children: [
        BottomNavigationBar(
          backgroundColor: primaryColor,
          onTap: onTappedBar,
          currentIndex: selected,
          items: [
            BottomNavigationBarItem(
                activeIcon: SvgPicture.asset("assets/icons/icon_home_on.svg"),
                icon: SvgPicture.asset("assets/icons/icon_home.svg"),
                label: "Home"),
            BottomNavigationBarItem(
              activeIcon: SvgPicture.asset("assets/icons/about_icon_on.svg"),
              icon: SvgPicture.asset("assets/icons/about_icon_non_avtive.svg"),
              label: "About",
            )
          ],
          selectedItemColor: white,
          unselectedItemColor: primary950,
        ),
      ]),
    );
  }
}
