import 'package:applikasi_translator_indonesia_sunda/provider/clipboard_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/image_preview_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/translation_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => TranslationProvider()),
        ChangeNotifierProvider(create: (context) => ClipboardProvider()),
        ChangeNotifierProvider(create: (context) => ImagePreviewProvider()),
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'My App',
        home: SplashScreen(),
      ),
    );
  }
}
