import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class GoogleMLVisionApi {
  // Menyimpan kunci API sebagai konstanta
  static const String apiKey = 'AIzaSyAQlWwx9KXj7BdEj2jIT-Mp6NVCyNX-WOg';

  // Fungsi untuk mengenali teks dari gambar yang di-encode dalam format Base64
  static Future<String> recogniseText(String base64Image) async {
    const endpoint =
        'https://vision.googleapis.com/v1/images:annotate?key=$apiKey';

    // Menyiapkan payload permintaan untuk di-post ke API
    final Map<String, dynamic> requestBody = {
      "requests": [
        {
          "image": {"content": base64Image},
          "features": [
            {"type": "TEXT_DETECTION"}
          ]
        }
      ]
    };

    // Melakukan permintaan HTTP ke Google Cloud Vision API
    final response = await http.post(Uri.parse(endpoint),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(requestBody));

    // Mengekstrak teks dari respons JSON
    final decodedResponse = jsonDecode(response.body);
    final textAnnotations = decodedResponse['responses'][0]['textAnnotations'];
    final text = textAnnotations != null && textAnnotations.isNotEmpty
        ? textAnnotations[0]['description']
        : 'teks tidak ditemukan';

    return text;
  }

  // Fungsi untuk meng-encode gambar ke dalam format Base64
  static String encodeImageToBase64(File imageFile) {
    // Membaca file gambar sebagai byte
    final imageBytes = imageFile.readAsBytesSync();
    // Meng-encode byte gambar menjadi string Base64
    return base64Encode(imageBytes);
  }

  // Fungsi untuk mendecode gambar dari format Base64 menjadi byte
  static File decodeImageFromBase64(String base64Image, String outputPath) {
    // Mendecode string Base64 menjadi byte gambar
    final imageBytes = base64Decode(base64Image);
    // Menyimpan byte gambar ke dalam file
    final File imageFile = File(outputPath);
    imageFile.writeAsBytesSync(imageBytes);
    return imageFile;
  }
}
