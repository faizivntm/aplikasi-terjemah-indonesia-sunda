import 'dart:async';
import 'package:applikasi_translator_indonesia_sunda/mybottom_navigation_bar.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/size_config.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 5), () {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: ((context) => const MyBottomNavigationBar())));
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Material(
      child: Container(
        decoration: const BoxDecoration(
          color: primaryColor,
        ),
        child: Center(
          child: Wrap(
            children: [
              Column(
                children: [
                  ClipOval(
                    child: SvgPicture.asset(
                      "assets/images/logo_apps.svg",
                      height: getProportionateScreenHeight(100),
                      width: getProportionateScreenWidth(100),
                      fit: BoxFit.fill,
                    ),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(10),
                  ),
                  Text(
                    "Aplikasi Terjemah",
                    style: primaryTextStyle.copyWith(
                        color: white, fontSize: 25, fontWeight: bold),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Indonesia - Sunda",
                    style: primaryTextStyle.copyWith(
                        fontSize: 12, color: white, fontWeight: bold),
                    textAlign: TextAlign.center,
                  ),
                  const Text(
                    'V.1.0',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 14,
                      color: primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(20),
                  ),
                  const CircularProgressIndicator(
                    color: lightGrey,
                    backgroundColor: white,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
