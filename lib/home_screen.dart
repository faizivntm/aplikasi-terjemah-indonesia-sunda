import 'package:applikasi_translator_indonesia_sunda/camera_screen.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/clipboard_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/translation_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/size_config.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:translator/translator.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

var textController = TextEditingController();

class _HomeScreenState extends State<HomeScreen> {
  String textTranslate = '';
  String temp = '';
  String displayFrom = 'Indonesia';
  String displayTo = 'Sunda';
  String codeFrom = 'id';
  String codeTo = 'su';

  void tukarBahasa() {
    setState(() {
      temp = displayFrom;
      displayFrom = displayTo;
      displayTo = temp;

      temp = codeFrom;
      codeFrom = codeTo;
      codeTo = temp;

      temp = textController.text;
      textController.text = textTranslate;
      textTranslate = temp;

      if (textController.text != '') {
        setState(() {
          translate();
        });
      }
    });
  }

  Future<void> translate() async {
    final translator = GoogleTranslator();
    final textToBeTranslated = textController.text;
    try {
      final translation = await translator.translate(textToBeTranslated,
          from: codeFrom, to: codeTo);
      setState(() {
        textTranslate = translation.text;
      });
    } catch (error) {
      print('Error translating: $error');
    }
  }

  @override
  void initState() {
    super.initState();
    textController.text =
        Provider.of<TranslationProvider>(context, listen: false).inputText;
    translate();
  }

  void handleTextControllerChange() {
    // final inputText = textController.text;
    // Provider.of<TranslationProvider>(context, listen: false)
    //     .setInputText(inputText);
    setState(() {
      translate();
    });
  }

  File? image;

  @override
  Widget build(BuildContext context) {
    Provider.of<TranslationProvider>(context, listen: false)
        .setInputText(textController.text);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: white,
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: getProportionateScreenHeight(150),
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: getProportionateScreenHeight(30)),
                child: Column(
                  children: [
                    SizedBox(
                      height: getProportionateScreenHeight(50),
                    ),
                    Center(
                      child: Container(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        height: getProportionateScreenHeight(150),
                        width: getProportionateScreenWidth(307),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: lightGrey,
                        ),
                        child: Column(
                          children: [
                            SizedBox(
                              height: getProportionateScreenHeight(10),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  displayFrom,
                                  style: primaryTextStyle.copyWith(
                                    fontSize: 20,
                                    fontWeight: bold,
                                    color: primary950,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(5),
                            ),
                            Container(
                              height: getProportionateScreenHeight(1),
                              width: getProportionateScreenWidth(275),
                              color: primary950,
                            ),
                            Expanded(
                              child: Consumer<TranslationProvider>(
                                builder: (context, provider, child) {
                                  return TextFormField(
                                    maxLines: null,
                                    textInputAction: TextInputAction.newline,
                                    decoration: InputDecoration(
                                      hintText: 'type something ...',
                                      hintStyle: primaryTextStyle.copyWith(
                                        color: primary700,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    controller: textController,
                                    onChanged: (value) {
                                      return handleTextControllerChange();
                                    },
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        tukarBahasa();
                      },
                      icon: SvgPicture.asset(
                        "assets/icons/switch_icon.svg",
                        height: getProportionateScreenHeight(16),
                        width: getProportionateScreenWidth(13),
                      ),
                    ),
                    Center(
                      child: Container(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        height: getProportionateScreenHeight(150),
                        width: getProportionateScreenWidth(307),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: lightGrey,
                        ),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  displayTo,
                                  style: primaryTextStyle.copyWith(
                                    fontSize: 20,
                                    fontWeight: bold,
                                    color: primary950,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Consumer<TranslationProvider>(
                                        builder: (context, provider, child) {
                                      return IconButton(
                                        onPressed: () async {
                                          FocusScope.of(context).unfocus();
                                          final clipboardProvider =
                                              Provider.of<ClipboardProvider>(
                                            context,
                                            listen: false,
                                          );
                                          clipboardProvider.copyToClipboard(
                                              textTranslate, context);
                                        },
                                        icon: SvgPicture.asset(
                                          "assets/icons/icon_clipboard.svg",
                                          height:
                                              getProportionateScreenHeight(20),
                                          width:
                                              getProportionateScreenWidth(20),
                                        ),
                                      );
                                    }),
                                  ],
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(5),
                            ),
                            Container(
                              height: getProportionateScreenHeight(1),
                              width: getProportionateScreenWidth(275),
                              color: primary950,
                            ),
                            Container(
                              height: getProportionateScreenHeight(90),
                              width: getProportionateScreenWidth(270),
                              alignment: Alignment.center,
                              child: Consumer<TranslationProvider>(
                                builder: (context, provider, child) {
                                  return textController.text.isNotEmpty
                                      ? Text(
                                          textTranslate,
                                          style: primaryTextStyle.copyWith(
                                            fontSize: 15,
                                            fontWeight: light,
                                            color: primary700,
                                          ),
                                        )
                                      : Text(
                                          'Your Translate',
                                          style: primaryTextStyle.copyWith(
                                            fontSize: 15,
                                            fontWeight: light,
                                            color: primary700,
                                          ),
                                        );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(15),
                    ),
                    SizedBox(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: getProportionateScreenHeight(85),
                            width: getProportionateScreenWidth(80),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: lightGrey,
                            ),
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  onPressed: () {},
                                  icon: SvgPicture.asset(
                                    "assets/icons/icon_text.svg",
                                    height: getProportionateScreenHeight(22),
                                    width: getProportionateScreenWidth(19),
                                  ),
                                ),
                                Text(
                                  "Text",
                                  style: primaryTextStyle.copyWith(
                                    fontSize: 15,
                                    fontWeight: bold,
                                    color: primary950,
                                  ),
                                )
                              ],
                            )),
                          ),
                          SizedBox(
                            width: getProportionateScreenWidth(20),
                          ),
                          Container(
                            height: getProportionateScreenHeight(85),
                            width: getProportionateScreenWidth(80),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: primary700,
                            ),
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const CameraScreen()));
                                  },
                                  icon: SvgPicture.asset(
                                    "assets/icons/icon_camera.svg",
                                    height: getProportionateScreenHeight(22),
                                    width: getProportionateScreenWidth(19),
                                  ),
                                ),
                                Text(
                                  "Camera",
                                  style: primaryTextStyle.copyWith(
                                    fontSize: 15,
                                    fontWeight: bold,
                                    color: white,
                                  ),
                                )
                              ],
                            )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
