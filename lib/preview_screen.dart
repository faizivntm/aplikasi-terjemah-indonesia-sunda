import 'dart:io';

import 'package:applikasi_translator_indonesia_sunda/api/google_ml_vision.dart';
import 'package:applikasi_translator_indonesia_sunda/home_screen.dart';
import 'package:applikasi_translator_indonesia_sunda/mybottom_navigation_bar.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/image_preview_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/translation_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/size_config.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PreviewScreen extends StatefulWidget {
  @override
  State<PreviewScreen> createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {
  String text = "";
  void setText(String newText) {
    setState(() {
      text = newText;
    });
  }

  @override
  Widget build(BuildContext context) {
    final imagePath = context.watch<ImagePreviewProvider>().imagePath;
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: getProportionateScreenHeight(60),
          centerTitle: true,
          backgroundColor: primaryColor,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text(
            "CAMERA",
            style: TextStyle(
              fontSize: 20,
              fontStyle: FontStyle.normal,
            ),
            textAlign: TextAlign.center,
          ),
          automaticallyImplyLeading: true,
        ),
        body: Center(
          child: Stack(children: [
            Image.file(File(imagePath)),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                color: primaryColor,
                height: getProportionateScreenHeight(80),
                width: MediaQuery.of(context).size.width,
                child: IconButton(
                    onPressed: () async {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      );

                      // Membuat file gambar dari path
                      final imageFile = File(imagePath);

                      // Meng-encode gambar ke Base64
                      final base64Image =
                          GoogleMLVisionApi.encodeImageToBase64(imageFile);

                      // Mengenali teks dari gambar yang di-encode
                      String text =
                          await GoogleMLVisionApi.recogniseText(base64Image);

                      Provider.of<TranslationProvider>(context, listen: false)
                          .setInputText(text);
                      textController.text = text;

                      Navigator.of(context).pop();

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MyBottomNavigationBar(),
                          ));
                    },
                    icon: const Icon(
                      Icons.check,
                      color: primary950,
                      size: 50,
                    )),
              ),
            )
          ]),
        ));
  }
}
