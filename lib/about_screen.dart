import 'package:applikasi_translator_indonesia_sunda/utils/size_config.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  bool isLastIndex = false;
  AboutScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            child: Center(
              child: Image.asset(
                'assets/images/logo_unnas.png',
                width: getProportionateScreenWidth(160),
                height: getProportionateScreenHeight(140),
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(18),
          ),
          Text(
            "UNIVERSITAS NASIONAL",
            style: primaryTextStyle.copyWith(
              fontWeight: light,
              fontSize: 13,
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(5),
          ),
          SizedBox(
            height: getProportionateScreenHeight(50),
            width: getProportionateScreenWidth(300),
            child: Text(
              "Aplikasi ini dibuat sebagai salah satu bahan penelitian untuk mendukung penulisan skripsi.",
              style: primaryTextStyle.copyWith(
                fontWeight: light,
                fontSize: 13,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: getProportionateScreenHeight(1),
            width: getProportionateScreenWidth(200),
            color: primary950,
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          SizedBox(
            height: getProportionateScreenHeight(60),
            width: getProportionateScreenWidth(300),
            child: Text(
              "Algoritma Encoder Decoder pada Aplikasi Penerjemah Bahasa Indonesia - Sunda dengan Metode Optical Character Recognition Berbasis Android.",
              style: primaryTextStyle.copyWith(
                fontWeight: bold,
                fontSize: 13,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Container(
            height: getProportionateScreenHeight(1),
            width: getProportionateScreenWidth(250),
            color: primary950,
          ),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Text(
            "Muhammad Rizki",
            style: primaryTextStyle.copyWith(
              fontWeight: light,
              fontSize: 13,
            ),
          ),
          Text(
            "207064516158",
            style: primaryTextStyle.copyWith(
              fontWeight: light,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }
}
