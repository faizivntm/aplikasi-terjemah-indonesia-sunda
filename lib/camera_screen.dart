//pada page ini merupakan keseluruhan untuk mengatur fitur kamera pada aplikasi

import 'dart:io';
import 'package:applikasi_translator_indonesia_sunda/preview_screen.dart';
import 'package:applikasi_translator_indonesia_sunda/provider/image_preview_provider.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/size_config.dart';
import 'package:applikasi_translator_indonesia_sunda/utils/theme.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class CameraScreen extends StatefulWidget {
  const CameraScreen({Key? key});
  @override
  State<CameraScreen> createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  CameraController? cameraController;
  List cameras = [];
  int selectedCameraIndex = 0;
  File? image;

  @override
  void initState() {
    super.initState();
    availableCameras().then((value) {
      cameras = value;
      if (cameras.length > 0) {
        selectedCameraIndex = 0;
        initCamera(cameras[selectedCameraIndex]).then((_) {});
      } else {
        print("Tidak ada kamera");
      }
    }).catchError((e) {
      print(e.code);
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future initCamera(CameraDescription cameraDescription) async {
    if (cameraController != null) {
      await cameraController!.dispose();
    }

    cameraController =
        CameraController(cameraDescription, ResolutionPreset.high);
    cameraController!.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });
    if (cameraController!.value.hasError) {
      print("Camera Eror");
    }
    try {
      await cameraController!.initialize();
    } catch (e) {
      print("Camera Eror $e");
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: getProportionateScreenHeight(60),
          centerTitle: true,
          backgroundColor: primaryColor,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text(
            "CAMERA",
            style: TextStyle(
              fontSize: 20,
              fontFamily: 'Montserrat',
              fontStyle: FontStyle.normal,
            ),
            textAlign: TextAlign.center,
          ),
          automaticallyImplyLeading: true,
        ),
        backgroundColor: primary950,
        body: SizedBox(
            child: Stack(children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: cameraPreview(),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: primaryColor,
              height: getProportionateScreenHeight(80),
              width: MediaQuery.of(context).size.width,
              child: IconButton(
                onPressed: () {
                  onCapture(context);
                },
                icon: SvgPicture.asset(
                  "assets/icons/icon_camera.svg",
                  height: getProportionateScreenHeight(55),
                  width: getProportionateScreenWidth(55),
                ),
              ),
            ),
          )
        ])));
  }

  Widget cameraPreview() {
    if (cameraController == null || !cameraController!.value.isInitialized) {
      return const Center(
        child: Text(
          "Loading",
          style: TextStyle(color: white),
        ),
      );
    }

    return CameraPreview(
      cameraController!,
    );
  }

  getCameraLensIcon(lensDirection) {
    switch (lensDirection) {
      case CameraLensDirection.back:
        return CupertinoIcons.switch_camera;
      case CameraLensDirection.front:
        return CupertinoIcons.switch_camera_solid;
      case CameraLensDirection.external:
        return CupertinoIcons.switch_camera_solid;
      default:
        return Icons.device_unknown;
    }
  }

  onSwitchCamera() {
    selectedCameraIndex =
        selectedCameraIndex < cameras.length - 1 ? selectedCameraIndex + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    initCamera(selectedCamera);
  }

  onCapture(context) async {
    try {
      final picture = await cameraController!.takePicture();
      Provider.of<ImagePreviewProvider>(context, listen: false)
          .setImagePath(picture.path);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (_) => PreviewScreen()),
      );
    } catch (e) {
      print('$e');
    }
  }
}
